
console.log('load @ ' + new Date());
var MAP = null;
var STATIONS = null;
var LAYER_CONTROL = null;
var BIKE_LAYER = null;
var OSM_LAYER = null;

async function refresh_bikes() {
    console.log(`refreshing bikes`);
    const stations_url = 'https://gbfs.lyft.com/gbfs/2.3/bkn/en/station_information.json';
    const stations_req = await fetch(stations_url);
    const stations_rsp = await stations_req.json();
    let stations = {};  // key is station_id
    for (const station of stations_rsp.data.stations) {
        stations[station.station_id] = {
            coord: [station.lat, station.lon],
            name: station.name,
            capacity: station.capacity
        };
    }

    const avail_url = 'https://gbfs.citibikenyc.com/gbfs/en/station_status.json';
    const avail_req = await fetch(avail_url);
    const avail_rsp = await avail_req.json();
    for (const msg of avail_rsp.data.stations) {
        const station = stations[msg.station_id];
        if (!station) {
            console.warn('Got msg w/ unknown station id', msg);
            continue
        }
        if (station.is_renting === false && station.is_returning === false) {
            console.log('Skipping defunct station ', msg);
            continue
        }
        L.marker(station.coord)
            .addTo(BIKE_LAYER)
            .bindPopup(`
                <h3> ${station.name} </h3>
                <table>
                    <tr> <th> Bikes </th> <td> ${msg.num_bikes_available} </td> </tr>
                    <tr> <th> Ebikes </th> <td> ${msg.num_ebikes_available} </td> </tr>
                </table>
                `);
    }

}

function toggle_darkmode() {
    const darkVal = document.getElementById('darkmode-checkbox').checked;
    console.log('Dark mode set to ' + darkVal);
    if (darkVal) { } else { }
}

function locate() {
    MAP.locate({
        setView: true,
        timeout: 5000,
        maximumAge: 5000
    });
}

function located(pos) {
    console.log(pos);
    L.marker(pos.latlng)
        .addTo(MAP)
        .bindPopup(`<p>As of ${new Date(pos.timestamp)}</p>`);
}

function main() {
    console.log('main');

    BIKE_LAYER = L.layerGroup();
    OSM_LAYER = L.tileLayer(
        'https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attributes: '<3 OSM'
        });
    MAP = L.map('map', {
        center: [40.7, -74],
        zoom: 16,
        layers: [ OSM_LAYER, BIKE_LAYER ]
    });
    LAYER_CONTROL = L.control.layers({}, { 'Bike Docks': BIKE_LAYER }).addTo(MAP);
    MAP.on('locationfound', located);
}

function demo() {
    const center = [40.748333, -73.985278];

    const map = L.map('map');
    map.setView(center, 16);

    const url = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png';
    const tiles = L.tileLayer(url, {
        maxZoom: 19,
        attribution: 'OpenStreetMap ATTR>'
    }).addTo(map);

    const marker = L.marker(center)
        .addTo(map)
        .bindPopup('<b>Hell world</b>')
        .openPopup();

    const circle = L.circle([51.508, -0.11], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 500
    }).addTo(map).bindPopup('I am a circle.');

    const polygon = L.polygon([
        [51.509, -0.08],
        [51.503, -0.06],
        [51.51, -0.047]
    ]).addTo(map).bindPopup('I am a polygon.');

    const popup = L.popup()
        .setLatLng([40.75, -73.98])
        .setContent('I am a standalone popup.')
        .openOn(map);

    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent(`You clicked the map at ${e.latlng.toString()}`)
            .openOn(map);
    }

    map.on('click', onMapClick);
    map.on('locationfound', located);
}

